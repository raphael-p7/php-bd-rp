<?php

class Game
{
    public function cardsGenerator($cardsNbr)
    {

        if (!isset($_SESSION['cards'])) {
            $cards = [];
            $colors = array('pique', 'coeur', 'trèfle', 'carreau');

            if ($cardsNbr == 32)
                $cardsNbrBegin = 7;
            else if ($cardsNbr == 52)
                $cardsNbrBegin = 2;
            else
                echo "<h1>ARRÊTE DE VOULOIR TOUT CASSER</h1>";

            for ($i = $cardsNbrBegin; $i <= 14; $i++) {
                foreach ($colors as $color) {
                    $cards[] = [
                        'value' => $i,
                        'color' => $color
                    ];
                }
            }
            shuffle($cards);
            // return $cards;
        }

        // CARDS DISTRIBUTION
        $splitArray = $cardsNbr / 2;
        $cardsSplited = array_chunk($cards, $splitArray, true);
        $_SESSION['playerOne'] = $cardsSplited[0];
        $_SESSION['computer'] = $cardsSplited[1];

        // ONE TURN
        $cardPick = array_pop($cardsSplited[0]);
        echo "<p>Your card :";
        var_dump($cardPick);
        $computerCardPick = array_pop($cardsSplited[1]);
        echo "<p>Computer card :";
        var_dump($computerCardPick);


    }
}

