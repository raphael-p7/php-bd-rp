<?php
function getLang() : string {

  // Recup langue navigateur
  $locale = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
  // SI langue pas défini -> FR
  if ($locale == "")
      $locale = 'fr-FR';
  // Recup deux premiers caracteres de la chaine
  $locale = substr("$locale", 0, 2);
  // Renvoi fonction
  return $locale;
}
