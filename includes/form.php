<?php



$nom = isset($nom) ? $nom : "";
$prenom = isset($prenom) ? $prenom : "";
$email = isset($email) ? $email : "";
?>

<section class="container">
    <form method="post" action="index.php?page=registration">
        <div class = form-group>
            <label for="nom">Nom&nbsp</label>
            <input name="nom" type="text" value="<?=$nom?>" class="form-input" placeholder="Michel"/>
        </div>
        <div class = form-group>
            <label for="prenom">Prénom&nbsp</label>
            <input name="prenom" type="text" value="<?=$prenom?>" class="form-input" placeholder="Michel"/>
        </div>
        <div class = form-group>
            <label for="email">Mail&nbsp</label>
            <input name="email" type="mail" value="<?=$email?>"class="form-input" placeholder="michel-le-vrai@michel.team"/>
        </div>
        <div class = form-group>
            <label for="mdp">Mot de passe&nbsp</label>
            <input name="mdp" type="password" class="form-input" placeholder="Entrez votre mot de passe..."/>
        </div>
        <div class = btn-group>
            <div class="reset-group">
                <i class="fas fa-long-arrow-alt-left"></i>
                <input type="reset" value="Effacer" class="btn-reset" />
            </div>
            <input type="submit" value="Valider" class="btn"/>
        </div>
        <input type="hidden" name="validation" />
    </form>
</section>