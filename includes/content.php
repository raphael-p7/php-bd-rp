<?php

if (isset($_SESSION['login']) && $_SESSION['login'] == 1){
  $prenom = $_SESSION['prenom'];
  $nom = $_SESSION['nom'];
  echo "<h1>Coucou $prenom $nom~</h1>";
}

$content = glob('./includes/*.inc.php');
$page = isset($_GET['page']) ? $_GET['page'] : "";

$page = './includes/' . $page . '.inc.php';

$page = in_array($page, $content) ? $page : './includes/home.inc.php';

require $page;
