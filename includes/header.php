<?php
if (isset($_SESSION['login']) && $_SESSION['login'] == 1)
    $logValue = "logout";
else
    $logValue = "login";
?>

<header>

    <nav>
        <div class="brand">
            <a href="index.php?page=home" class="logo">MICHEL POK'HAIR.</a>
        </div>

        <div class="nav-menu">
            <ul>
                <li><a href="index.php?page=home" title="Page d'accueil"><span>Home</span></a></li>
                <li><a href="index.php?page=news" title="Actualités"><span>News</span></a></li>
                <?php
                if (isset($_SESSION['login']) && $_SESSION['login'] == 1)
                    echo "<li><a href=\"index.php?page=game&size=32\" title=\"Game\"><span>Cards Game</span></a></li>";
                ?>
                <li><a href="index.php?page=<?=$logValue?>" title="<?=ucfirst($logValue)?>"><span><?=ucfirst($logValue)?></a></a></li>
                <?php
                if (!(isset($_SESSION['login']) && $_SESSION['login'] == 1))
                    echo "<li><a href=\"index.php?page=registration\" title=\"Registration\"><span>Register</span></a></li>";
                ?>

            </ul>
        </div>
    </nav>

</header>
