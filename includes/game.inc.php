<section class="container">

    <?php

    if (isset($_SESSION['login']) && $_SESSION['login'] == 1) {

        $partie = new Game();
        $partie->cardsGenerator($_GET['size']);

    }

    else {
        echo "<iframe src=\"https://giphy.com/embed/spfi6nabVuq5y/\" width=\"480\" height=\"392\" frameBorder=\"0\" class=\"giphy-embed\" allowFullScreen></iframe>";
    }

    ?>

    <div class="button-row">
        <a href="http://localhost/php-bd-rp/index.php?page=game&size=32" target="_self" class="btn-card card-32">32 cards</a>
        <a href="http://localhost/php-bd-rp/index.php?page=game&size=52" target="_self" class="btn-card card-52">52 cards</a>
    </div>

    <a href="#" class="btn-card">Play</a>

</section>
