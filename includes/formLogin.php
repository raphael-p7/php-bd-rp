<?php

$email = isset($email) ? $email : "";
$mdp = isset($mdp) ? $mdp : "";
?>

<section class="container">
    <form method="post" action="index.php?page=login">
        <div class="form-group">
            <label for="email">Mail&nbsp;</label>
            <input name="email" type="mail" value="<?=$email?>" placeholder="michel-le-vrai@michel.team" class="form-input"/>
        </div>
        <div class="form-group">
            <label for="mdp">Mot de passe&nbsp;</label>
            <input name="mdp" type="password" placeholder="Entrez votre mot de passe..." class="form-input"/>
        </div>
        <div class="btn-group">
            <div class="reset-group">
              <i class="fas fa-long-arrow-alt-left"></i>
              <input type="reset" value="Effacer" class="btn-reset" />
            </div>
            <input type="submit" value="Valider" class="btn" />
        </div>
        <input type="hidden" name="login" />
    </form>
</section>